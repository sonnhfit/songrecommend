import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "songrecommend.settings")

app = Celery("songrecommend")

app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()

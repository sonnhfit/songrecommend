from django.contrib import admin

from .models import BeatRating, BeatSong, MyUser, Record, RecordRating

admin.site.site_header = "Song recommend admin"


@admin.register(BeatSong, BeatRating, MyUser, Record, RecordRating)
class MyModelAdmin(admin.ModelAdmin):
    pass

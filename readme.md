### install
- docker
- docker-compose
- python 3.6 -> 3.9

### Build
```
docker-compose build
```

### Run

```
docker-compose up
```


### install pre-commit
- create your virtualenv
```
pip install -r requirements.txt
```
```
pre-commit install
```

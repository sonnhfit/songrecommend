from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import BeatSong
from .serializers import BeatSerializers


class BeatAPIView(APIView):
    def get(self, request):
        return Response(data="", status=status.HTTP_200_OK)

    def post(self, request):
        validate = BeatSerializers(data=request.data)
        if not validate.is_valid():
            return Response("invalid data", status=status.HTTP_400_BAD_REQUEST)

        beatId = validate.data["id"]
        beat_name = validate.data["name"]

        if not BeatSong.objects.filter(id=beatId).exists():
            BeatSong.objects.create(id=beatId, name=beat_name)

        return Response(data="sucess", status=status.HTTP_200_OK)


class RecordAPIView(APIView):
    def get(self, request):
        return Response(data="", status=status.HTTP_200_OK)

    def post(self, request):
        return Response(data="", status=status.HTTP_200_OK)

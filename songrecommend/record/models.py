from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from ..utils.models import TimeStampedModel


class MyUser(TimeStampedModel):
    id = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class BeatSong(TimeStampedModel):
    id = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class BeatRating(TimeStampedModel):
    user = models.ForeignKey(MyUser, models.PROTECT, "beat_ratings")
    beat_song = models.ForeignKey(BeatSong, models.PROTECT, "beat_ratings")
    rating = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)]
    )

    def __str__(self):
        return f"{self.user} ➙ {self.beat_song}"

    class Meta:
        unique_together = ["user", "beat_song"]


class Record(TimeStampedModel):
    id = models.CharField(primary_key=True, max_length=255)
    beat_song = models.ForeignKey(BeatSong, models.PROTECT, "records")
    user = models.ForeignKey(MyUser, models.PROTECT, "records")

    def __str__(self):
        return f"{self.beat_song} ({self.user})"


class RecordRating(TimeStampedModel):
    user = models.ForeignKey(MyUser, models.PROTECT, "record_ratings")
    record = models.ForeignKey(Record, models.PROTECT, "record_ratings")
    rating = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)]
    )

    def __str__(self):
        return f"{self.user} ➙ {self.record}"

    class Meta:
        unique_together = ["user", "record"]

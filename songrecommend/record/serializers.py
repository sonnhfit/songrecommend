from rest_framework import serializers


class BeatSerializers(serializers.Serializer):
    id = serializers.CharField(required=False)
    name = serializers.CharField(required=False)

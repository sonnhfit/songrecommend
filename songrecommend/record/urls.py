from django.urls import path

from .views import BeatAPIView

urlpatterns = [
    path("beat/", BeatAPIView.as_view()),
]
